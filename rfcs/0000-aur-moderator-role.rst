---
title: "Moderator role for AUR"
draft: true
---

Moderator role for the AUR
==========================

- Date proposed: 2023-06-27
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0000
  **update this number after RFC merge request has been filed**

Summary
-------

Create a dedicated role for the moderation of the AUR that supports and eventually replaces the Package Maintainers as the current moderators.

Motivation
----------

Currently, Arch Linux has 63 Package Maintainers. Historically known as Trusted Users (TU), they had two main responsibilties: Moderation of the AUR and moving popular AUR packages to the official Arch Linux repositories. With the merge of the TU-maintained `[community]` into `[extra]` and the renaming of TUs to Package Maintainers, their attention largely shifted away from the AUR moderation role. As open requests are piling up, this RFC suggests to split the old TU role into Package Maintainers and AUR Moderators.

Specification
-------------

Similar to forum moderators, the newly created role of AUR Moderators is to review requests on the AUR and accept or close them. Furthermore, they are allowed to suspend accounts that violate the AUR contribution guidelines. At the moment AUR web defines the following user types
* Normal user
* Trusted User
* Developer
* Trusted User & Developer

After the implemention of this RFC the user types will be changed to
* Normal user
* AUR Moderator
* Developer
* AUR Moderator & Developer

The moderation rights of Package Maintainers and Developers in the AUR are not changed, so that every Package Maintainer or Developer is an AUR Moderator, too.
AUR Moderators are recruited from the regular contributors of the AUR. To be promoted, users must secure the support of three Package Maintainers or Developers.
They can seek voting support directly by contacting staff members or can be suggested by the group of AUR Moderators.
Once three sponsors are found, the nominee writes an email signed with their SSH key for their AUR profile to aur-general. After the three staff members confirmed their sponsorship a discussion period of seven days starts. If not more than one staff member opposes the application, they are promoted to an AUR Moderator.

Drawbacks
---------

Packages in the AUR are not officially supported by Arch Linux. Adding a special role to handle AUR requests may lead to the impression that the AUR is an official package source. However, AUR Moderators will only watch over the AUR submission guidelines [1] that doesn't include a direct control of package quality.

Unresolved Questions
--------------------

None

Alternatives Considered
-----------------------

Package Maintainers should take care of the AUR more regularly. One possiblity is to name a group of up to five Package Maintainers who will focus on AUR requests for one week. A list with names is published as a markdown file every month.

[1] https://wiki.archlinux.org/title/AUR_submission_guidelines
